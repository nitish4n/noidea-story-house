<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clip extends Model
{
    protected $table = 'clips';
}
