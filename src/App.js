import React, {Fragment} from 'react';
import Navabr from './Components/Navbar';
import LoginPage from './container/Login';
import './App.css';
import {useSelector} from 'react-redux';
import {Switch , Route, useHistory, Redirect} from 'react-router-dom';
import Home from './container/Home/home';
function App() {
  const history = useHistory();

  const auth = useSelector(state => state.user);
  
  return (
    <div className="App">
      <Navabr />

      <Switch>
      <Route exact path="/" render={
          auth.authenticated ? history.push('/home')
          : ""
        }>
            Home
          </Route>
        {auth.authenticated ?
        <Fragment>
          <Route path="/home" component={Home} />
          </Fragment>
        :
          history.push('/login')
          
        }

        
        <Route path="/login" render={
          auth.authenticated ? history.push('/home')
          : ""
        } component={LoginPage} />
        
        
      </Switch>
    </div>
  );
}

export default App;
