import React, {useState} from 'react'
import TextField from '@material-ui/core/TextField';
import './login.css'
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import Button from '@material-ui/core/Button';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {Link} from 'react-router-dom';
import {auth, db, googleProvider} from '../../firebase';
import {add_user_action} from '../../redux/actions/UserAction';
import {useDispatch} from 'react-redux';

function Index() {
    const dispatch = useDispatch();
    const initial_credentials = {
        email:'',
        password:''
    }
    const [loginCredentials, setLoginCredentials] = useState(initial_credentials);

    const formHandler = (event) => {
        const {name, value} = event.target;
        setLoginCredentials({...loginCredentials, [name]: value})
    }

    const LoginSubmitFormHandler = (e) => {
        e.preventDefault();
        console.log(loginCredentials)
    }

    const googleSignIn =( e )=> {
        auth.signInWithPopup(googleProvider)
        .then(userAuth => {
            if(typeof window === undefined){
                alert('Unable to access Window Details')
            }
            
            localStorage.setItem('userData', JSON.stringify(userAuth.user));
            dispatch(add_user_action(userAuth.user))
        })
        .catch(err => alert(err))
    }
    return (
        <div className="login">
            <h2 className="login__welcomeText">Welcome Back : )</h2>
            <p>To stay connected with us. please login with your account credentials</p>
        
            <div className="login__area">
                <img src={process.env.PUBLIC_URL+ "man_listening_music.png"} alt="NISH logo" className="loginn__logo" />
                <div className="login__form">

                    <div className="login__email">
                    <MailOutlineIcon />
                    <TextField
                        id="outlined-secondary"
                        label="Email Id"
                        variant="outlined"
                        name="email"
                        color="secondary"
                        className="enter__email"
                        value={loginCredentials.email}
                        onChange={formHandler}
                    />
                    </div>

                    <div className="login__password">
                    <LockOpenIcon />
                    <TextField
                        id="outlined-secondary"
                        label="password"
                        variant="outlined"
                        name="password"
                        color="secondary"
                        type="password"
                        className="enter__email"
                        value={loginCredentials.password}
                        onChange={formHandler}
                    />
                    </div>
                    <Button className="login__submitButtin" onClick={LoginSubmitFormHandler} endIcon={<ExitToAppIcon />} >Log In</Button>
                    
                    <Link to="/forget-password"><Button>Forget Password</Button></Link>
                    <div className="login__registerOption">  
                        <p>Don't have account !! Not to worry <Link to="/register">Register here </Link> </p>
                    </div>
                    <hr />

                    <div className="social__login">
                        <Button onClick={googleSignIn}>
                            <img src="https://www.google.co.in/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png" alt="" />
                        </Button>
                        <Button>
                            <img src="https://static.xx.fbcdn.net/rsrc.php/y8/r/dF5SId3UHWd.svg" alt="" />
                        </Button>
                    </div>
                </div>
                 
            </div>
        </div>
    )
}

export default Index
