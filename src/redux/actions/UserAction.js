import {ADD_USER_AUTH} from '../contants/index';

export const add_user_action = (user) => ({
    type: ADD_USER_AUTH,
    payload: user
})