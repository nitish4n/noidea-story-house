import {ADD_USER_AUTH} from '../contants/index';

const INITIAL_STATE = {
    user: null,
    authenticated: false
}

const localData = localStorage.getItem('userData');
var Data_State = {};
if(localData){
    var Data_State = {
        user: JSON.parse(localData),
        authenticated: true
    }
}

const UserReducer = (state = localData ? Data_State : INITIAL_STATE, action) => {

    switch(action.type) {
        case ADD_USER_AUTH:
            return {
                ...state,
                authenticated:true,
                user: action.payload
            }
        default:
            return state
    }
}


export default UserReducer;