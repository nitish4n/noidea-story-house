import React, { Component } from 'react';
import {combineReducers, compose, createStore} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import UserReducer from './reducer/User';


const reducer = combineReducers({
    user: UserReducer
})

const store = createStore(reducer, composeWithDevTools());

export default store;