import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyDoVD4JhG9ycLbXK6C1xcyAjWdfu9COU9g",
    authDomain: "noidea-story-house.firebaseapp.com",
    databaseURL: "https://noidea-story-house.firebaseio.com",
    projectId: "noidea-story-house",
    storageBucket: "noidea-story-house.appspot.com",
    messagingSenderId: "805609043239",
    appId: "1:805609043239:web:07558d470cdcf3a82e2502",
    measurementId: "G-0G3L8NJRD3"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
export const db = firebase.firestore();

